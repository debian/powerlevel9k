Source: powerlevel9k
Section: utils
Priority: optional
Maintainer: Jonathan Carter <jcc@debian.org>
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.6.1
Homepage: https://github.com/bhilburn/powerlevel9k
Vcs-Git: https://salsa.debian.org/debian/powerlevel9k.git
Vcs-Browser: https://salsa.debian.org/debian/powerlevel9k

Package: zsh-theme-powerlevel9k
Architecture: all
Depends: fonts-powerline, zsh, ${misc:Depends}
Description: powerlevel9k is a theme for zsh which uses powerline fonts
 Get more out of your terminal. Be a badass. Impress everyone in
 'Screenshot Your Desktop' threads. Use powerlevel9k.
 .
 There are a number of Powerline ZSH themes available, now. The developers of
 this theme focus on four primary goals:
  - Give users a great out-of-the-box configuration with no additional
    configuration required.
  - Make customization easy for users who do want to tweak their prompt.
  - Provide useful segments that you can enable to make your prompt even more
    effective and helpful. It has prompt segments for everything from unit
    test coverage to your AWS instance.
  - Optimize the code for execution speed as much as possible. A snappy
    terminal is a happy terminal.
  - Powerlevel9k can be used to create both very useful and beautiful
    terminal environments.
 .
 To enable this theme for the current user, run:
   echo 'source  /usr/share/powerlevel9k/powerlevel9k.zsh-theme' >> ~/.zshrc
